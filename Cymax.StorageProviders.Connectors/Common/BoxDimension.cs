﻿using System.Xml.Serialization;

namespace Cymax.StorageProviders.Connectors
{
    /// <summary>
    /// Box Dimension
    /// </summary>
    public struct BoxDimension: IBoxDimension
    {
        /// <summary>
        /// Box Width in Meter
        /// </summary>
        [XmlElement("width")]
        public double Width { get; set; }

        /// <summary>
        /// Box Length in Meter
        /// </summary>
        [XmlElement("length")]
        public double Length { get; set; }

        //Box Heigth in Meter
        [XmlElement("height")]
        public double Height { get; set; }
    }
}
