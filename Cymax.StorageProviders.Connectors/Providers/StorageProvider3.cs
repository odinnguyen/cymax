﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Cymax.StorageProviders.Connectors
{
    /// <summary>
    /// Storage Provider 3
    /// </summary>
    public class StorageProvider3 : StorageProviderBase
    {
        /// <summary>
        /// Storage Provider's name
        /// </summary>
        const string COMPANY_NAME = "Company 3";

        /// <summary>
        /// Constructor
        /// </summary>
        public StorageProvider3()
            : this(new StorageProvider3Api(API_URL, USER_NAME, PASSWORD))
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name">Storage Provider's name</param>
        /// <param name="api">Storage Provider's Api</param>
        public StorageProvider3(IStorageProviderApi api)
            : base(COMPANY_NAME, api)
        {
        }

        /// <summary>
        /// Get Quote
        /// </summary>
        /// <param name="srcAddress">source postal address</param>
        /// <param name="destAddress">destination postal address</param>
        /// <param name="cartonDimensions">carton boxes dimenions</param>
        /// <returns>Quote</returns>
        public override async Task<decimal> RequestQuote(string srcAddress, string destAddress, List<BoxDimension> cartonDimensions)
        {
            var serializedString = await Api.GetQuoteAsync(srcAddress, destAddress, cartonDimensions);

            var xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(serializedString);
            var rootNode = xmlDocument.SelectSingleNode("xml");
            var quoteNode = rootNode.SelectSingleNode("quote");

            return Convert.ToDecimal(quoteNode.InnerText);
        }
    }
}
