﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cymax.StorageProviders.Connectors
{
    /// <summary>
    /// Interface for Storage Provider Api
    /// </summary>
    public interface IStorageProviderApi
    {
        /// <summary>
        /// Get Quote
        /// </summary>
        /// <param name="srcAddress">source postal address</param>
        /// <param name="destAddress">destination postal address</param>
        /// <param name="cartonDimensions">dimension of all boxes</param>
        /// <returns>serialized string result from http request</returns>
        public string GetQuote(string srcAddress, string destAddress, List<BoxDimension> cartonDimensions);

        /// <summary>
        /// Get Quote Async
        /// </summary>
        /// <param name="srcAddress">source postal address</param>
        /// <param name="destAddress">destination postal address</param>
        /// <param name="cartonDimensions">dimension of all boxes</param>
        /// <returns>serialized string result from http request</returns>
        public Task<string> GetQuoteAsync(string srcAddress, string destAddress, List<BoxDimension> cartonDimensions);
    }
}
