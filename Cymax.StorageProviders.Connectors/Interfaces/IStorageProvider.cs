﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cymax.StorageProviders.Connectors
{
    /// <summary>
    /// Interface for storage provider
    /// </summary>
    public interface IStorageProvider
    {
        /// <summary>
        /// Storage Provider's Name
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Get Quote
        /// </summary>
        /// <param name="srcAddress">source postal address</param>
        /// <param name="destAddress">destination postal address</param>
        /// <param name="cartonDimensions">carton boxes dimenions</param>
        /// <returns>Quote</returns>
        Task<decimal> RequestQuote(string srcAddress, string destAddress, List<BoxDimension> cartonDimensions);
    }
}
