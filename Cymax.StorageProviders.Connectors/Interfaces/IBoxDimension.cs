﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cymax.StorageProviders.Connectors
{
    public interface IBoxDimension
    {
        /// <summary>
        /// Box Width in Meter
        /// </summary>
        public double Width { get; }

        /// <summary>
        /// Box Length in Meter
        /// </summary>
        public double Length { get; }

        //Box Heigth in Meter
        public double Height { get; }
    }
}
