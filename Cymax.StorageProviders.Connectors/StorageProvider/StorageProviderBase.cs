﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cymax.StorageProviders.Connectors
{
    /// <summary>
    /// Storage Provider Base Class
    /// </summary>
    public abstract class StorageProviderBase : IStorageProvider
    {
        /// <summary>
        /// Storage Provider's Api Url
        /// </summary>
        protected const string API_URL = "http://localhost:21000";

        /// <summary>
        /// Hardcoded User Name
        /// </summary>
        public const string USER_NAME = "admin@cymax.com";

        /// <summary>
        /// Hardcoded Password
        /// </summary>
        public const string PASSWORD = "Password@123";

        /// <summary>
        /// Storage Provider's Name
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Storage Provider's Api
        /// </summary>
        protected IStorageProviderApi Api { get; private set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name">Storage Provider's Name</param>
        /// <param name="api">Storage Provider's Api</param>
        public StorageProviderBase(string name, IStorageProviderApi api)
        {
            Name = name;
            Api = api;
        }

        /// <summary>
        /// Get Quote
        /// </summary>
        /// <param name="srcAddress">source postal address</param>
        /// <param name="destAddress">destination postal address</param>
        /// <param name="cartonDimensions">carton boxes dimenions</param>
        /// <returns>Quote</returns>
        public abstract Task<decimal> RequestQuote(string srcAddress, string destAddress, List<BoxDimension> cartonDimensions);
    }
}
