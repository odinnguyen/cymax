﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cymax.StorageProviders.Connectors
{
    /// <summary>
    /// Storage Provider's Api Base Class
    /// </summary>
    public abstract class StorageProviderApiBase: IStorageProviderApi
    {
        /// <summary>
        /// Api Url
        /// </summary>
        protected Uri Url { get; private set; }

        /// <summary>
        /// User Name
        /// </summary>
        protected string UserName { get; private set; }

        /// <summary>
        /// Password
        /// </summary>
        protected string Password { get; private set; }

        /// <summary>
        /// Rest Web Service
        /// </summary>
        protected RestWsClient RestWebService { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="url">Api Url</param>
        public StorageProviderApiBase(string url, string userName, string password)
        {
            Url = new Uri(url);
            UserName = userName;
            Password = password;
            RestWebService = new RestWsClient(url, userName, password);
        }

        /// <summary>
        /// Get Quote
        /// </summary>
        /// <param name="srcAddress">source postal address</param>
        /// <param name="destAddress">destination postal address</param>
        /// <param name="cartonDimensions">dimension of all boxes</param>
        /// <returns>serialized string result from http request</returns>
        public abstract string GetQuote(string srcAddress, string destAddress, List<BoxDimension> cartonDimensions);

        /// <summary>
        /// Get Quote
        /// </summary>
        /// <param name="srcAddress">source postal address</param>
        /// <param name="destAddress">destination postal address</param>
        /// <param name="cartonDimensions">dimension of all boxes</param>
        /// <returns>serialized string result from http request</returns>
        public abstract Task<string> GetQuoteAsync(string srcAddress, string destAddress, List<BoxDimension> cartonDimensions);
    }
}
