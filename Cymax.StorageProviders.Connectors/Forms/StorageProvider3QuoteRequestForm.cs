﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Cymax.StorageProviders.Connectors
{
    /// <summary>
    /// Storage Provider 3 Quote Request Form
    /// </summary>
    public class StorageProvider3QuoteRequestForm
    {
        /// <summary>
        /// Source's address
        /// </summary>
        [XmlElement("source")]
        public string Source { get; set; }

        /// <summary>
        /// Destination's Address
        /// </summary>
        [XmlElement("destination")]
        public string Destination { get; set; }

        /// <summary>
        /// Packages' dimensions
        /// </summary>
        [XmlElement("packages")]
        public List<BoxDimension> Packages { get; set; }
    }
}
