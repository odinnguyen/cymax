﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Serialization;

namespace Cymax.StorageProviders.Connectors
{
    /// <summary>
    /// RESTful Web Service Helper class
    /// </summary>
    public class RestWsClient
    {
        /// <summary>
        /// Request time out
        /// </summary>
        public const int TIME_OUT = 30000; //30 seconds

        /// <summary>
        /// Api endpoint url
        /// </summary>
        private readonly string Endpoint;

        /// <summary>
        /// User Name
        /// </summary>
        protected string UserName { get; private set; }

        /// <summary>
        /// Password
        /// </summary>
        protected string Password { get; private set; }

        /// <summary>
        /// Authentication Token
        /// </summary>
        public (string Token, DateTime ExpiryTime) AuthToken { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="endpoint">
        /// Web Service root URL.
        /// </param>
        public RestWsClient(string endpoint, string userName, string password)
        {
            // Store endpoint with '/' as the last character to make it easier
            // to combine the paths with the endpoint later.
            if (!endpoint.EndsWith("/")) endpoint += "/";

            this.Endpoint = endpoint;
            this.UserName = userName;
            this.Password = password;
        }

        private void Login()
        {
            var request = WebRequest.Create(Endpoint + "Authenticate/login");
            request.Method = System.Net.Http.HttpMethod.Post.Method;
            request.ContentType = MediaTypeNames.Application.Json;

            using (var requestStream = request.GetRequestStream())
            using (var streamWriter = new StreamWriter(requestStream))
                streamWriter.Write(JsonConvert.SerializeObject(new
                {
                    username = UserName,
                    password = Password
                }));

            using (var response = (HttpWebResponse)request.GetResponse())
            using (var responseStream = response.GetResponseStream())
            {
                var jsonString = new StreamReader(responseStream).ReadToEnd();
                dynamic auth = JsonConvert.DeserializeObject(jsonString);
                AuthToken = (auth.token, Convert.ToDateTime(auth.expiration).ToLocalTime());
            }
        }

        /// <summary>
        /// Perform http request
        /// </summary>
        /// <typeparam name="TReturn">return type</typeparam>
        /// <param name="method">http method</param>
        /// <param name="path">url path from endpoint</param>
        /// <param name="contentType">request body content type</param>
        /// <param name="content">request body content</param>
        /// <returns>request output</returns>
        public object PerformRequest(string method, string path, string contentType, object content)
        {
            if (string.IsNullOrWhiteSpace(AuthToken.Token) || AuthToken.ExpiryTime < DateTime.Now.AddMilliseconds(-TIME_OUT))
                Login();

            // Endpoint has a trailing slash so remove leading slash from
            // the nodePath if such exists.
            if (path[0] == '/') path = path.Substring(1);
            var uri = Endpoint + path;

            var request = WebRequest.Create(uri);
            request.ContentType = contentType;
            request.Timeout = TIME_OUT;

            //Set the method
            request.Method = method;
            
            //Set the authentication header
            request.Headers["Authorization"] = "Bearer " + AuthToken.Token;

            if (content != null)
                using (var requestStream = request.GetRequestStream())
                using (var streamWriter = new StreamWriter(requestStream))
                    if (contentType == MediaTypeNames.Application.Json)
                        streamWriter.Write(JsonConvert.SerializeObject(content));
                    else if (contentType == MediaTypeNames.Application.Xml)
                    {
                        var xmlSerializer = new XmlSerializer(content.GetType());
                        xmlSerializer.Serialize(streamWriter, content);
                    }

            using (var response = (HttpWebResponse)request.GetResponse())
            using (var responseStream = response.GetResponseStream())
                return new StreamReader(responseStream).ReadToEnd();
        }
    }
}
