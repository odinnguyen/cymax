﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace Cymax.StorageProviders.Connectors
{
    /// <summary>
    /// Storage Provider 1's Api
    /// </summary>
    public class StorageProvider1Api : StorageProviderApiBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="url">Api Url</param>
        public StorageProvider1Api(string url, string userName, string password) 
            : base(url, userName, password)
        {
        }

        /// <summary>
        /// Get Quote
        /// </summary>
        /// <param name="srcAddress">source postal address</param>
        /// <param name="destAddress">destination postal address</param>
        /// <param name="cartonDimensions">dimension of all boxes</param>
        /// <returns>serialized string result from http request</returns>
        public override string GetQuote(string srcAddress, string destAddress, List<BoxDimension> cartonDimensions)
        {
            if (string.IsNullOrWhiteSpace(srcAddress))
                throw new ArgumentException("source postal address is required", nameof(srcAddress));

            if (string.IsNullOrWhiteSpace(destAddress))
                throw new ArgumentException("destination postal address is required", nameof(destAddress));

            if (cartonDimensions == null || !cartonDimensions.Any())
                throw new ArgumentException("boxes's dimensions is required", nameof(cartonDimensions));


            return RestWebService.PerformRequest(
                System.Net.Http.HttpMethod.Post.Method,
                "StorageProvider1/Quote",
                MediaTypeNames.Application.Json,
                new
                {
                    ContactAddress = srcAddress,
                    WarehouseAddress = destAddress,
                    PackageDimensions = cartonDimensions
                }).ToString();
        }

        /// <summary>
        /// Get Quote Async
        /// </summary>
        /// <param name="srcAddress">source postal address</param>
        /// <param name="destAddress">destination postal address</param>
        /// <param name="cartonDimensions">dimension of all boxes</param>
        /// <returns>serialized string result from http request</returns>
        public override async Task<string> GetQuoteAsync(string srcAddress, string destAddress, List<BoxDimension> cartonDimensions)
        {
            return GetQuote(srcAddress, destAddress, cartonDimensions);
        }
    }
}
