﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cymax.StorageProviders.Connectors
{
    /// <summary>
    /// To manage all Storage Providers
    /// </summary>
    public class StorageProviderManager
    {
        /// <summary>
        /// Get all storage providers
        /// </summary>
        /// <returns>storage providers</returns>
        public static IList<IStorageProvider> GetAllProviders()
        {
            return new List<IStorageProvider> { new StorageProvider1(), new StorageProvider2(), new StorageProvider3() };
        }
    }
}
