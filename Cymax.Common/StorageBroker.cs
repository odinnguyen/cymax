﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cymax.Common
{
    using Cymax.StorageProviders.Connectors;
    /// <summary>
    /// Storage Broker
    /// </summary>
    public class StorageBroker
    {
        /// <summary>
        /// Storage Providers' List
        /// </summary>
        private IList<IStorageProvider> Providers;

        /// <summary>
        /// Constructor
        /// </summary>
        public StorageBroker()
            : this(StorageProviderManager.GetAllProviders())
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="providers">Storage Providers' List</param>
        public StorageBroker(IList<IStorageProvider> providers)
        {
            Providers = providers;
        }

        /// <summary>
        /// Get best quote
        /// </summary>
        /// <param name="srcAddress">source postal address</param>
        /// <param name="destAddress">destination postal address</param>
        /// <param name="cartonDimensions">carton boxes dimenions</param>
        /// <returns>Storage Provider's Name, best quote (least expensive)</returns>
        public (string CompanyName, decimal Quote) GetBestQuote(string srcAddress, string destAddress, List<BoxDimension> cartonDimensions)
        {
            (string CompanyName, decimal Quote) bestQuote = (string.Empty, decimal.MaxValue);

            var blockingCollection = new BlockingCollection<(string CompanyName, decimal Quote)>();
            var tasks = new List<Task>();
            foreach (var provider in Providers)
            {
                var task = new Task(() =>
                    provider.RequestQuote(srcAddress, destAddress, cartonDimensions)
                    .ContinueWith((result) => blockingCollection.TryAdd((provider.Name, result.Result)))
                    .Wait()
                );
                task.Start();
                tasks.Add(task);
            }

            Task.WhenAll(tasks.ToArray()).ContinueWith((result) => blockingCollection.CompleteAdding());

            try
            {
                while (!blockingCollection.IsAddingCompleted)
                {
                    var quote = blockingCollection.Take();
                    if (quote.Quote < bestQuote.Quote)
                        bestQuote = quote;
                }
            }
            catch (InvalidOperationException)
            {
                // An InvalidOperationException means that Take() was called on a completed collection
            }

            return bestQuote;
        }
    }
}
