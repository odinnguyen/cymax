# README #

## What is this repository for? ##

This repository is a code assignment from Cymax for job seeker  

## What Cymax Asked ##

At-Home Project: Requesting multiple API  

The concept is to request several companies' API for offers and select the best deal.  

### Conditions ###
- No UI expected.  
- No SQL required.  
- Must be unit-tested.  

### Process Input ###
- one set of data {{source address}, {destination address}, [{carton dimensions}]}  
- Multiple API using the same data with different signatures  

### Process Output ###
- All API respond with the same data in different formats  
- Process must query, then select the lowest offer and return it in the least amount of time  
 
### Sample APIs, each with its own url and credentials ###

API1 (JSON)  
- Input {contact address, warehouse address, package dimensions:[]}  
- Output {total}  

API2 (JSON)  
- Input {consignee, consignor, cartons:[]}  
- Output {amount}  

API3 (XML)  
- Input <xml><source/><destination/><packages><package/></packages></xml>  
- Output <xml><quote/></xml>  

## How to Use ##

### Deploy External.Api to your local IIS at "localhost:21000" ###

Use "Cymax" to name both your Application Pool and Site  

You can follow this guide on how to deploy to IIS: https://youtu.be/VKTZWZI1AhE  
- Disclaimer: At the moment, the owner enables sharing for his video.  

When setup publishing profile for "Web Server (IIS)" in Visual Studio:  
- Server: localhost  
- Site name: Cymax  
- Destination URL: http://localhost:21000/swagger/index.html  

### Install SQL Express ###

Download and install SQL server express: https://www.microsoft.com/en-us/Download/details.aspx?id=101064  
Download latest SSMS for viewing your db: https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-ver15  

### Migrate Db ###

- Using Visual Studio > Package Manager Console. Run "update-database"  
Note: the first user was seeded: username=admin@cymax.com, password=Password@123  
- Connect to your localhost\SQLEXPRESS using SSMS. Add Windows User "IIS APPPOOL\Cymax" to your db "Cymax.External.Api-53bc9b9d-9d6a-45d4-8429-2a2761773502". Grant this user read and write right  
Note: Cymax here is your AppPool's name

### Test ###

- Run all Unit Tests
- This unit test will validate if your installation of above steps is correct: Cymax.Test > Cymax.Common.Test > StorageBrokerTest > StorageBroker_BestQuote  