﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Cymax.StorageProviders.Connectors.Test
{
    using Cymax.StorageProviders.Connectors;

    /// <summary>
    /// Mock Storage Provider 3
    /// </summary>
    public class MockStorageProvider3 : StorageProvider3
    {
        /// <summary>
        /// Mock Storage Provider 3 Api
        /// </summary>
        public class MockStorageProvider3Api : StorageProvider3Api
        {
            /// <summary>
            /// Price per Cubic Meter
            /// </summary>
            public double PricePerCubicMeter { get; private set; }

            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="pricePerCubicMeter">Price per Cubic Meter</param>
            public MockStorageProvider3Api(double pricePerCubicMeter)
                : base("http://www.NotReal.com/api", "", "")
            {
                PricePerCubicMeter = pricePerCubicMeter;
            }

            /// <summary>
            /// Get Quote
            /// </summary>
            /// <param name="srcAddress">source postal address</param>
            /// <param name="destAddress">destination postal address</param>
            /// <param name="cartonDimensions">carton boxes dimenions</param>
            /// <returns>Quote</returns>
            public override string GetQuote(string srcAddress, string destAddress, List<BoxDimension> cartonDimensions)
            {
                var totalCubicMeter = cartonDimensions.Sum(d => d.Height * d.Width * d.Length);

                var xml = 
                    $@"<xml>
	                    <sourceAddress>
		                    {srcAddress}
	                    </sourceAddress>
	                    <destinationAddress>
		                    {destAddress}
	                    </destinationAddress>
	                    <totalCubicMeter>
		                    {totalCubicMeter}
	                    </totalCubicMeter>
	                    <pricePerCubicMeter>
		                    {PricePerCubicMeter}
	                    </pricePerCubicMeter>
	                    <quote>
		                    {Math.Round(totalCubicMeter * PricePerCubicMeter, 2)}
	                    </quote>
                    </xml>";

                return xml;
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="pricePerCubicMeter">Price per Cubic Meter</param>
        public MockStorageProvider3(double pricePerCubicMeter)
            : base(new MockStorageProvider3Api(pricePerCubicMeter))
        {

        }
    }
}
