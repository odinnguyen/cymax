﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cymax.StorageProviders.Connectors.Test
{
    using Cymax.Common;
    using Cymax.StorageProviders.Connectors;

    /// <summary>
    /// Mock Storage Provider 1
    /// </summary>
    public class MockStorageProvider1: StorageProvider1
    {
        /// <summary>
        /// Mock Storage Provider 1 Api
        /// </summary>
        public class MockStorageProvider1Api : StorageProvider1Api
        {
            /// <summary>
            /// Price per Cubic Meter
            /// </summary>
            public double PricePerCubicMeter{ get; private set; }

            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="pricePerCubicMeter">Price per Cubic Meter</param>
            public MockStorageProvider1Api(double pricePerCubicMeter) 
                : base("http://www.NotReal.com/api", "", "")
            {
                PricePerCubicMeter = pricePerCubicMeter;
            }

            /// <summary>
            /// Get Quote
            /// </summary>
            /// <param name="srcAddress">source postal address</param>
            /// <param name="destAddress">destination postal address</param>
            /// <param name="cartonDimensions">carton boxes dimenions</param>
            /// <returns>Quote</returns>
            public override string GetQuote(string srcAddress, string destAddress, List<BoxDimension> cartonDimensions)
            {
                if (string.IsNullOrWhiteSpace(srcAddress))
                    throw new ArgumentException("source postal address is required", nameof(srcAddress));

                if (string.IsNullOrWhiteSpace(destAddress))
                    throw new ArgumentException("destination postal address is required", nameof(destAddress));

                if (cartonDimensions == null || !cartonDimensions.Any())
                    throw new ArgumentException("boxes's dimensions is required", nameof(cartonDimensions));

                var totalCubicMeter = cartonDimensions.Sum(d => d.Height * d.Width * d.Length);
                var json = new
                {
                    sourceAddress = srcAddress,
                    destinationAddress = destAddress,
                    totalCubicMeter = totalCubicMeter,
                    pricePerCubicMeter = PricePerCubicMeter,
                    total = Math.Round(totalCubicMeter * PricePerCubicMeter, 2)
                };

                return JsonConvert.SerializeObject(json);
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="pricePerCubicMeter">Price per Cubic Meter</param>
        public MockStorageProvider1(double pricePerCubicMeter)
            : base (new MockStorageProvider1Api(pricePerCubicMeter))
        {

        }
    }
}
