﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace Cymax.StorageProviders.Connectors.Test
{
    using Cymax.Common;
    using Cymax.StorageProviders.Connectors;

    /// <summary>
    /// Test Storage Provider Api Base Class
    /// </summary>
    public abstract class StorageProviderApiTestBase
    {
        /// <summary>
        /// Mock URL
        /// </summary>
        public const string API_URL = "http://www.NotReal.com/api";

        /// <summary>
        /// Test No Source Address
        /// </summary>
        public void TestNoSourceAddress(IStorageProviderApi api)
        {
            var destinationAddess = "destinationAddess";
            var boxesDimensions = new List<BoxDimension> { new BoxDimension { Width = 1, Length = 2, Height = 3 } };

            Assert.ThrowsException<ArgumentException>(() => api.GetQuote(null, destinationAddess, boxesDimensions));
            Assert.ThrowsException<ArgumentException>(() => api.GetQuote(string.Empty, destinationAddess, boxesDimensions));
            Assert.ThrowsException<ArgumentException>(() => api.GetQuote("   ", destinationAddess, boxesDimensions));
        }

        /// <summary>
        /// Test No Destination Address
        /// </summary>
        public void TestNoDestinationAddress(IStorageProviderApi api)
        {
            var sourceAddess = "sourceAddess";
            var boxesDimensions = new List<BoxDimension> { new BoxDimension { Width = 1, Length = 2, Height = 3 } };

            Assert.ThrowsException<ArgumentException>(() => api.GetQuote(sourceAddess, null, boxesDimensions));
            Assert.ThrowsException<ArgumentException>(() => api.GetQuote(sourceAddess, string.Empty, boxesDimensions));
            Assert.ThrowsException<ArgumentException>(() => api.GetQuote(sourceAddess, "   ", boxesDimensions));
        }

        /// <summary>
        /// Test No Boxes's Dimensions
        /// </summary>
        public void TestNoBoxDimension(IStorageProviderApi api)
        {
            var sourceAddess = "sourceAddess";
            var destinationAddess = "destinationAddess";

            Assert.ThrowsException<ArgumentException>(() => api.GetQuote(sourceAddess, destinationAddess, null));
            Assert.ThrowsException<ArgumentException>(() => api.GetQuote(sourceAddess, destinationAddess, new List<BoxDimension>()));
        }
    }
}
