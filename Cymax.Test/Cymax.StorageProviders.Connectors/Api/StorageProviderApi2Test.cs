﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Cymax.StorageProviders.Connectors.Test
{
    using Cymax.StorageProviders.Connectors;

    /// <summary>
    /// Test Storage Provider Api 2
    /// </summary>
    [TestClass]
    public class StorageProviderApi2Test : StorageProviderApiTestBase
    {
        /// <summary>
        /// Test No Source Address
        /// </summary>
        [TestMethod]
        public void StorageProviderApi2_NoSourceAddress()
        {
            TestNoSourceAddress(new StorageProvider2Api(API_URL, StorageProvider2.USER_NAME, StorageProvider2.PASSWORD));
        }

        /// <summary>
        /// Test No Destination Address
        /// </summary>
        [TestMethod]
        public void StorageProviderApi2_NoDestinationAddress()
        {
            TestNoDestinationAddress(new StorageProvider2Api(API_URL, StorageProvider2.USER_NAME, StorageProvider2.PASSWORD));
        }

        /// <summary>
        /// Test No Boxes's Dimensions
        /// </summary>
        [TestMethod]
        public void StorageProviderApi2_NoBoxDimension()
        {
            TestNoBoxDimension(new StorageProvider2Api(API_URL, StorageProvider2.USER_NAME, StorageProvider2.PASSWORD));
        }
    }
}
