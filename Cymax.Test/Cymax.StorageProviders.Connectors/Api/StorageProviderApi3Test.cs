﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Cymax.StorageProviders.Connectors.Test
{
    using Cymax.StorageProviders.Connectors;

    /// <summary>
    /// Test Storage Provider Api 3
    /// </summary>
    [TestClass]
    public class StorageProviderApi3Test : StorageProviderApiTestBase
    {
        /// <summary>
        /// Test No Source Address
        /// </summary>
        [TestMethod]
        public void StorageProviderApi3_NoSourceAddress()
        {
            TestNoSourceAddress(new StorageProvider3Api(API_URL, StorageProvider3.USER_NAME, StorageProvider3.PASSWORD));
        }

        /// <summary>
        /// Test No Destination Address
        /// </summary>
        [TestMethod]
        public void StorageProviderApi3_NoDestinationAddress()
        {
            TestNoDestinationAddress(new StorageProvider3Api(API_URL, StorageProvider3.USER_NAME, StorageProvider3.PASSWORD));
        }

        /// <summary>
        /// Test No Boxes's Dimensions
        /// </summary>
        [TestMethod]
        public void StorageProviderApi3_NoBoxDimension()
        {
            TestNoBoxDimension(new StorageProvider3Api(API_URL, StorageProvider3.USER_NAME, StorageProvider3.PASSWORD));
        }
    }
}
