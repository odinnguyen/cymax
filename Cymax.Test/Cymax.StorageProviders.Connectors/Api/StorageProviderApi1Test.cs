﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Cymax.StorageProviders.Connectors.Test
{

    /// <summary>
    /// Test Storage Provider Api 1
    /// </summary>
    [TestClass]
    public class StorageProviderApi1Test : StorageProviderApiTestBase
    {
        /// <summary>
        /// Test No Source Address
        /// </summary>
        [TestMethod]
        public void StorageProviderApi1_NoSourceAddress()
        {
            TestNoSourceAddress(new StorageProvider1Api(API_URL, StorageProvider1.USER_NAME, StorageProvider1.PASSWORD));
        }

        /// <summary>
        /// Test No Destination Address
        /// </summary>
        [TestMethod]
        public void StorageProviderApi1_NoDestinationAddress()
        {
            TestNoDestinationAddress(new StorageProvider1Api(API_URL, StorageProvider1.USER_NAME, StorageProvider1.PASSWORD));
        }

        /// <summary>
        /// Test No Boxes's Dimensions
        /// </summary>
        [TestMethod]
        public void StorageProviderApi1_NoBoxDimension()
        {
            TestNoBoxDimension(new StorageProvider1Api(API_URL, StorageProvider1.USER_NAME, StorageProvider1.PASSWORD));
        }
    }
}
