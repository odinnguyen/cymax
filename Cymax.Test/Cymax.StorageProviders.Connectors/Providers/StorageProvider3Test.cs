﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Cymax.StorageProviders.Connectors.Test
{
    using Cymax.StorageProviders.Connectors;

    /// <summary>
    /// Test Storage Provider 3
    /// </summary>
    [TestClass]
    public class StorageProvider3Test: StorageProviderTestBase
    {
        /// <summary>
        /// Test Deserialization
        /// </summary>
        [TestMethod]
        public void StorageProvider3_TestDeserialization()
        {
            TestDeserialization(new StorageProvider3(new MockStorageProvider3.MockStorageProvider3Api(PricePerCubicMeter)));
        }
    }
}
