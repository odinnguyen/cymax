﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cymax.StorageProviders.Connectors.Test
{
    using Cymax.Common;
    using Cymax.StorageProviders.Connectors;

    /// <summary>
    /// Storage Provider Test Base
    /// </summary>
    public abstract class StorageProviderTestBase
    {
        /// <summary>
        /// Source postal address
        /// </summary>
        protected string SourceAddess { get; private set; } = "sourceAddess";

        /// <summary>
        /// Destination postal address
        /// </summary>
        protected string DestinationAddess { get; private set; } = "destinationAddess";

        /// <summary>
        /// Boxes' dimensions
        /// </summary>
        protected List<BoxDimension> BoxesDimensions { get; private set; } = 
            new List<BoxDimension> {
                new BoxDimension { Width = 1, Length = 2, Height = 3 },
                new BoxDimension { Width = 1.5, Length = 1.5, Height = 4 }
            };

        /// <summary>
        /// Price per cubic meter
        /// </summary>
        protected double PricePerCubicMeter { get; private set; } = 100.00;

        /// <summary>
        /// Test Deserialization
        /// </summary>
        public void TestDeserialization(IStorageProvider provider)
        {
            var quote = provider.RequestQuote(SourceAddess, DestinationAddess, BoxesDimensions).Result;
            var totalCubicMeter = BoxesDimensions.Sum(d => d.Length * d.Width * d.Height);
            var expected = Convert.ToDecimal(Math.Round(totalCubicMeter * PricePerCubicMeter, 2));
            Assert.AreEqual(expected, quote);
        }
    }
}
