﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Cymax.StorageProviders.Connectors.Test
{
    using Cymax.StorageProviders.Connectors;

    /// <summary>
    /// Test Storage Provider 2
    /// </summary>
    [TestClass]
    public class StorageProvider2Test: StorageProviderTestBase
    {
        /// <summary>
        /// Test Deserialization
        /// </summary>
        [TestMethod]
        public void StorageProvider2_TestDeserialization()
        {
            TestDeserialization(new StorageProvider2(new MockStorageProvider2.MockStorageProvider2Api(PricePerCubicMeter)));
        }
    }
}
