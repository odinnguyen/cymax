﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Cymax.StorageProviders.Connectors.Test
{
    using Cymax.StorageProviders.Connectors;

    /// <summary>
    /// Test Storage Provider 1
    /// </summary>
    [TestClass]
    public class StorageProvider1Test: StorageProviderTestBase
    {
        /// <summary>
        /// Test Deserialization
        /// </summary>
        [TestMethod]
        public void StorageProvider1_TestDeserialization()
        {
            TestDeserialization(new StorageProvider1(new MockStorageProvider1.MockStorageProvider1Api(PricePerCubicMeter)));
        }
    }
}
