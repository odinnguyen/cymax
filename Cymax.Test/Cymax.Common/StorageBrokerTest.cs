using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace Cymax.Common.Test
{
    using Cymax.Common;
    using Cymax.StorageProviders.Connectors;
    using Cymax.StorageProviders.Connectors.Test;

    /// <summary>
    /// Test Storage Broker
    /// </summary>
    [TestClass]
    public class StorageBrokerTest
    {
        /// <summary>
        /// Test Get Best Quote
        /// </summary>
        [TestMethod]
        public void StorageBroker_BestQuote_Mock()
        {
            var storageBroker = new StorageBroker(
                new List<IStorageProvider> {
                    new MockStorageProvider1(99.99),
                    new MockStorageProvider2(119.99),
                    new MockStorageProvider3(89.99),
                }
            );

            var sourceAddess = "sourceAddess";
            var destinationAddess = "destinationAddess";
            var boxesDimensions = new List<BoxDimension> { new BoxDimension { Width = 1, Length = 2, Height = 3 } };
            var bestQuote = storageBroker.GetBestQuote(sourceAddess, destinationAddess, boxesDimensions);
            var expected = Convert.ToDecimal(Math.Round(89.99 * 6, 2));

            Assert.AreEqual(expected, bestQuote.Quote);
        }

        /// <summary>
        /// Test Get Best Quote
        /// </summary>
        [TestMethod]
        public void StorageBroker_BestQuote()
        {
            var storageBroker = new StorageBroker(
                new List<IStorageProvider> {
                    new StorageProvider1(),
                    new StorageProvider2(),
                    new StorageProvider3()
                }
            );

            var sourceAddess = "sourceAddess";
            var destinationAddess = "destinationAddess";
            var boxesDimensions = new List<BoxDimension> { new BoxDimension { Width = 1, Length = 2, Height = 3 } };
            var bestQuote = storageBroker.GetBestQuote(sourceAddess, destinationAddess, boxesDimensions);

            Assert.AreNotEqual(decimal.MaxValue, bestQuote.Quote);
        }
    }
}
