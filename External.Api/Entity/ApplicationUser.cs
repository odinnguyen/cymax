﻿using Microsoft.AspNetCore.Identity;
using System;

namespace External.Api.Entity
{
    /// <summary>
    /// Identity User
    /// </summary>
    public class ApplicationUser : IdentityUser
    {
        /// <summary>
        /// The hardcoded first ever user for seeding purpose
        /// </summary>
        public static readonly (Guid Id, string Email) FirstUser = (new Guid("c7d0e97d-c4c6-4e28-af7a-55cd45afd0b8"), "admin@cymax.com");
    }
}
