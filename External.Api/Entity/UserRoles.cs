﻿using Microsoft.AspNetCore.Identity;
using System;

namespace External.Api.Entity
{
    /// <summary>
    /// User Roles
    /// </summary>
    public class UserRoles
    {
        /// <summary>
        /// List of all User Roles
        /// </summary>
        public class Roles
        {
            /// <summary>
            /// "User" role
            /// </summary>
            public const string User = "User";

            /// <summary>
            /// "Admin" role
            /// </summary>
            public const string Admin = "Admin";
        }

        /// <summary>
        /// "User" Role Id and name
        /// </summary>
        public static readonly (Guid Id, string Name) User = (new Guid("fab4fac1-c546-41de-aebc-a14da6895711"), Roles.User);

        /// <summary>
        /// "Admin" Role Id and name
        /// </summary>
        public static readonly (Guid Id, string Name) Admin = (new Guid("35f5ffaf-b950-4b18-93bf-8dc14b7b376c"), Roles.Admin);
    }
}
