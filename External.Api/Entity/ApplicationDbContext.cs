﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace External.Api.Entity
{
    /// <summary>
    /// Identity Db Context
    /// </summary>
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public ApplicationDbContext()
            : base()
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="options">Db Context Options</param>
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        /// <summary>
        /// Override OnModelCreating
        /// </summary>
        /// <param name="builder">Model Builder</param>
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            SeedRoles(builder);
            SeedUsers(builder);
            SeedUserRoles(builder);
        }

        /// <summary>
        /// Seed Users
        /// </summary>
        /// <param name="builder">Model Builder</param>
        private void SeedUsers(ModelBuilder builder)
        {
            var appUser = new ApplicationUser
            {
                Id = ApplicationUser.FirstUser.Id.ToString(),
                UserName = ApplicationUser.FirstUser.Email,
                NormalizedUserName = ApplicationUser.FirstUser.Email.ToUpperInvariant(),
                Email = ApplicationUser.FirstUser.Email,
                NormalizedEmail = ApplicationUser.FirstUser.Email.ToUpperInvariant(),
                EmailConfirmed = true,
                LockoutEnabled = false
            };

            //set user password
            var ph = new PasswordHasher<ApplicationUser>();
            appUser.PasswordHash = ph.HashPassword(appUser, "Password@123");

            //seed user
            builder.Entity<ApplicationUser>().HasData(appUser);
        }

        /// <summary>
        /// Seed Roles
        /// </summary>
        /// <param name="builder">Model Builder</param>
        private void SeedRoles(ModelBuilder builder)
        {
            builder.Entity<IdentityRole>().HasData(
                new IdentityRole() { Id = Entity.UserRoles.User.Id.ToString(), Name = Entity.UserRoles.User.Name, ConcurrencyStamp = Guid.NewGuid().ToString(), NormalizedName = Entity.UserRoles.User.Name.ToUpperInvariant() },
                new IdentityRole() { Id = Entity.UserRoles.Admin.Id.ToString(), Name = Entity.UserRoles.Admin.Name, ConcurrencyStamp = Guid.NewGuid().ToString(), NormalizedName = Entity.UserRoles.Admin.Name.ToUpperInvariant() }
            );
        }

        /// <summary>
        /// Seed Users' Roles
        /// </summary>
        /// <param name="builder">Model Builder</param>
        private void SeedUserRoles(ModelBuilder builder)
        {
            builder.Entity<IdentityUserRole<string>>().HasData(
                new IdentityUserRole<string>() { RoleId = Entity.UserRoles.User.Id.ToString(), UserId = ApplicationUser.FirstUser.Id.ToString() },
                new IdentityUserRole<string>() { RoleId = Entity.UserRoles.Admin.Id.ToString(), UserId = ApplicationUser.FirstUser.Id.ToString() }
            );
        }
    }
}
