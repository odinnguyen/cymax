﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace External.Api.ViewModels
{
    /// <summary>
    /// Storage Provider 3 Quote Request Form
    /// </summary>
    public class StorageProvider3QuoteRequestForm
    {
        /// <summary>
        /// Source's address
        /// </summary>
        [XmlElement("source")]
        public string Source { get; set; }

        /// <summary>
        /// Destination's Address
        /// </summary>
        [XmlElement("destination")]
        public string Destination { get; set; }

        /// <summary>
        /// Packages' dimensions
        /// </summary>
        [XmlElement("packages")]
        public List<BoxDimension> Packages { get; set; }
    }

    /// <summary>
    /// Storage Provider 3 Quote
    /// </summary>
    [XmlRoot("xml")]
    public class StorageProvider3Quote: StorageProviderQuoteBase
    {
        /// <summary>
        /// Source's address
        /// </summary>
        [XmlElement("source")]
        public string Source { get; set; }

        /// <summary>
        /// Destination's Address
        /// </summary>
        [XmlElement("destination")]
        public string Destination { get; set; }

        /// <summary>
        /// Total price
        /// </summary>
        [XmlElement("quote")]
        public decimal Quote { get; set; }
    }
}
