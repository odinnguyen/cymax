﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace External.Api.ViewModels
{
    /// <summary>
    /// Storage Provider 1 Quote Request Form
    /// </summary>
    public class StorageProvider1QuoteRequestForm
    {
        /// <summary>
        /// Requester Contact Address
        /// </summary>
        [JsonRequired] public string ContactAddress { get; set; }

        /// <summary>
        /// Requested warehouse address
        /// </summary>
        [JsonRequired] public string WarehouseAddress { get; set; }

        /// <summary>
        /// Packages' dimensions
        /// </summary>
        [JsonRequired] public List<BoxDimension> PackageDimensions { get; set; }
    }

    /// <summary>
    /// Storage Provider 1 Quote
    /// </summary>
    public class StorageProvider1Quote: StorageProviderQuoteBase
    {
        /// <summary>
        /// Requester Contact Address
        /// </summary>
        [JsonRequired] public string ContactAddress { get; set; }

        /// <summary>
        /// Requested warehouse address
        /// </summary>
        [JsonRequired] public string WarehouseAddress { get; set; }

        /// <summary>
        /// Total price
        /// </summary>
        [JsonRequired] public decimal Total { get; set; }
    }
}
