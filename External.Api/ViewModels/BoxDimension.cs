﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace External.Api.ViewModels
{
    /// <summary>
    /// Box dimension
    /// </summary>
    public class BoxDimension
    {
        /// <summary>
        /// Box's Width
        /// </summary>
        [XmlElement("width")]
        public double Width { get; set; }

        /// <summary>
        /// Box's Length
        /// </summary>
        [XmlElement("length")]
        public double Length { get; set; }

        /// <summary>
        /// Box's Height
        /// </summary>
        [XmlElement("height")]
        public double Height { get; set; }
    }
}
