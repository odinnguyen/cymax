﻿namespace External.Api.ViewModels.Authentication
{
    /// <summary>
    /// Response Model
    /// </summary>
    public class Response
    {
        /// <summary>
        /// Status Name
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Message
        /// </summary>
        public string Message { get; set; }
    }
}
