﻿using System.ComponentModel.DataAnnotations;

namespace External.Api.ViewModels.Authentication
{
    /// <summary>
    /// Login Model
    /// </summary>
    public class LoginModel
    {
        /// <summary>
        /// Username
        /// </summary>
        [Required(ErrorMessage = "User Name is required")]
        public string Username { get; set; }

        /// <summary>
        /// Password
        /// </summary>

        [Required(ErrorMessage = "Password is required")]
        public string Password { get; set; }
    }
}
