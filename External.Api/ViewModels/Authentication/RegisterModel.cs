﻿using System.ComponentModel.DataAnnotations;

namespace External.Api.ViewModels.Authentication
{
    /// <summary>
    /// User registration model
    /// </summary>
    public class RegisterModel
    {
        /// <summary>
        /// Username
        /// </summary>
        [Required(ErrorMessage = "User Name is required")]
        public string Username { get; set; }

        /// <summary>
        /// User's Email
        /// </summary>
        [EmailAddress]
        [Required(ErrorMessage = "Email is required")]
        public string Email { get; set; }

        /// <summary>
        /// Password
        /// </summary>
        [Required(ErrorMessage = "Password is required")]
        public string Password { get; set; }
    }
}
