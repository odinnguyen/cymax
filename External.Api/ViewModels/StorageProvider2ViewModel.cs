﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace External.Api.ViewModels
{
    /// <summary>
    /// Storage Provider 2 Quote Request Form
    /// </summary>
    public class StorageProvider2QuoteRequestForm
    {
        /// <summary>
        /// Consignee's address
        /// </summary>
        [JsonRequired] public string Consignee { get; set; }

        /// <summary>
        /// Consignor's Address
        /// </summary>
        [JsonRequired] public string Consignor { get; set; }

        /// <summary>
        /// Boxes' dimensions
        /// </summary>
        [JsonRequired] public List<BoxDimension> Cartons { get; set; }
    }

    /// <summary>
    /// Storage Provider 2 Quote
    /// </summary>
    public class StorageProvider2Quote: StorageProviderQuoteBase
    {
        /// <summary>
        /// Consignee's address
        /// </summary>
        [JsonRequired] public string Consignee { get; set; }

        /// <summary>
        /// Consignor's Address
        /// </summary>
        [JsonRequired] public string Consignor { get; set; }

        /// <summary>
        /// Total price
        /// </summary>
        [JsonRequired] public decimal Amount { get; set; }
    }
}
