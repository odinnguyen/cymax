﻿using Newtonsoft.Json;
using System;

namespace External.Api.ViewModels
{
    /// <summary>
    /// Storage Provider Quote Base Class
    /// </summary>
    public class StorageProviderQuoteBase
    {
        /// <summary>
        /// Quote Id, for future reference
        /// </summary>
        public Guid QuoteId { get; set; }

        /// <summary>
        /// Total cubic meter of all boxes
        /// </summary>
        public double TotalCubicMeter { get; set; }

        /// <summary>
        /// Price per cubic meter
        /// </summary>
        public double PricePerCubicMeter { get; set; }

        /// <summary>
        /// Discount percentage
        /// </summary>
        public string Discount { get; set; }

        /// <summary>
        /// Expriry date of the quotation
        /// </summary>
        public DateTime ValidTill { get; set; }
    }
}
