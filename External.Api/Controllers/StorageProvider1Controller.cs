﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Net.Mime;

namespace External.Api.Controllers
{
    using External.Api.Entity;
    using External.Api.ViewModels;

    /// <summary>
    /// Storage Provider 1's Api
    /// </summary>
    [ApiController]
    [Route("[controller]/Quote")]
    public class StorageProvider1Controller : ControllerBase
    {
        /// <summary>
        /// Hard coded price per cubic meter
        /// </summary>
        const double PRICE_PER_CUBIC_METER = 99.99;

        /// <summary>
        /// Get quote for storage
        /// </summary>
        /// <param name="requestForm">request form</param>
        /// <returns>Quote</returns>
        [Authorize(Roles = UserRoles.Roles.User)]
        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json)]
        [Produces(MediaTypeNames.Application.Json)]
        public StorageProvider1Quote Post(StorageProvider1QuoteRequestForm requestForm)
        {
            var rng = new Random(Convert.ToInt32(DateTime.Now.Ticks % Int32.MaxValue));
            var disCount = Math.Round(rng.NextDouble(), 2);

            if (string.IsNullOrWhiteSpace(requestForm.ContactAddress))
                throw new ArgumentException("contact address is required", nameof(requestForm.ContactAddress));

            if (string.IsNullOrWhiteSpace(requestForm.WarehouseAddress))
                throw new ArgumentException("warehouse address is required", nameof(requestForm.WarehouseAddress));

            if (requestForm.PackageDimensions == null || !requestForm.PackageDimensions.Any())
                throw new ArgumentException("packages' dimension is required", nameof(requestForm.PackageDimensions));

            var totalCubicMeter = requestForm.PackageDimensions.Sum(d => d.Height * d.Width * d.Length);

            return new StorageProvider1Quote
            {
                QuoteId = Guid.NewGuid(),
                ContactAddress = requestForm.ContactAddress,
                WarehouseAddress = requestForm.WarehouseAddress,
                TotalCubicMeter = totalCubicMeter,
                PricePerCubicMeter = PRICE_PER_CUBIC_METER,
                Discount = $"{(disCount * 100).ToString("0")}%",
                ValidTill = DateTime.Now.AddDays(1),
                Total = Convert.ToDecimal(Math.Round(totalCubicMeter * PRICE_PER_CUBIC_METER * (1 - disCount), 2))
            };
        }
    }
}
