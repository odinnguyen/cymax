﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Net.Mime;

namespace External.Api.Controllers
{
    using External.Api.Entity;
    using External.Api.ViewModels;

    /// <summary>
    /// Storage Provider 2's Api
    /// </summary>
    [ApiController]
    [Route("[controller]/Quote")]
    public class StorageProvider2Controller : ControllerBase
    {
        /// <summary>
        /// Hard coded price per cubic meter
        /// </summary>
        const double PRICE_PER_CUBIC_METER = 109.95;

        /// <summary>
        /// Get quote for storage
        /// </summary>
        /// <param name="requestForm">request form</param>
        /// <returns>Quote</returns>
        [Authorize(Roles = UserRoles.Roles.User)]
        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json)]
        [Produces(MediaTypeNames.Application.Json)]
        public StorageProvider2Quote Post(StorageProvider2QuoteRequestForm requestForm)
        {
            var rng = new Random(Convert.ToInt32(DateTime.Now.Ticks % Int32.MaxValue));
            var disCount = Math.Round(rng.NextDouble(), 2);

            if (string.IsNullOrWhiteSpace(requestForm.Consignee))
                throw new ArgumentException("consignee is required", nameof(requestForm.Consignee));

            if (string.IsNullOrWhiteSpace(requestForm.Consignor))
                throw new ArgumentException("consignor is required", nameof(requestForm.Consignor));

            if (requestForm.Cartons == null || !requestForm.Cartons.Any())
                throw new ArgumentException("cartons' dimension is required", nameof(requestForm.Cartons));

            var totalCubicMeter = requestForm.Cartons.Sum(d => d.Height * d.Width * d.Length);

            return new StorageProvider2Quote
            {
                QuoteId = Guid.NewGuid(),
                Consignee = requestForm.Consignee,
                Consignor = requestForm.Consignor,
                TotalCubicMeter = totalCubicMeter,
                PricePerCubicMeter = PRICE_PER_CUBIC_METER,
                Discount = $"{(disCount * 100).ToString("0")}%",
                ValidTill = DateTime.Now.AddDays(1),
                Amount = Convert.ToDecimal(Math.Round(totalCubicMeter * PRICE_PER_CUBIC_METER * (1 - disCount), 2))
            };
        }
    }
}
