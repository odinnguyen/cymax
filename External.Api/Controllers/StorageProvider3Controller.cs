﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters.Xml.Extensions;
using System;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace External.Api.Controllers
{
    using External.Api.Entity;
    using External.Api.ViewModels;

    /// <summary>
    /// Storage Provider 2's Api
    /// </summary>
    [ApiController]
    [Route("[controller]/Quote")]
    public class StorageProvider3Controller : ControllerBase
    {
        /// <summary>
        /// Hard coded price per cubic meter
        /// </summary>
        const double PRICE_PER_CUBIC_METER = 95.49;

        /// <summary>
        /// Get quote for storage
        /// </summary>
        /// <param name="requestForm">request form</param>
        /// <returns>Quote</returns>
        [Authorize(Roles = UserRoles.Roles.User)]
        [HttpPost]
        [Consumes(MediaTypeNames.Application.Xml)]
        [Produces(MediaTypeNames.Application.Xml)]
        public async Task<ActionResult> Post([FromXmlBody] StorageProvider3QuoteRequestForm requestForm)
        {
            var rng = new Random(Convert.ToInt32(DateTime.Now.Ticks % Int32.MaxValue));
            var disCount = Math.Round(rng.NextDouble(), 2);

            if (string.IsNullOrWhiteSpace(requestForm.Source))
                throw new ArgumentException("source address is required", nameof(requestForm.Source));

            if (string.IsNullOrWhiteSpace(requestForm.Destination))
                throw new ArgumentException("destination address is required", nameof(requestForm.Destination));

            if (requestForm.Packages == null || !requestForm.Packages.Any())
                throw new ArgumentException("packages' dimension is required", nameof(requestForm.Packages));

            var totalCubicMeter = requestForm.Packages.Sum(d => d.Height * d.Width * d.Length);

            var xmlSerializer = new XmlSerializer(typeof(StorageProvider3Quote));
            xmlSerializer.Serialize(Response.Body, new StorageProvider3Quote
            {
                QuoteId = Guid.NewGuid(),
                Source = requestForm.Source,
                Destination = requestForm.Destination,
                TotalCubicMeter = totalCubicMeter,
                PricePerCubicMeter = PRICE_PER_CUBIC_METER,
                Discount = $"{(disCount * 100).ToString("0")}%",
                ValidTill = DateTime.Now.AddDays(1),
                Quote = Convert.ToDecimal(Math.Round(totalCubicMeter * PRICE_PER_CUBIC_METER * (1 - disCount), 2))
            });

            return Ok();
        }
    }
}
